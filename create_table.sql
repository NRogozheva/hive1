USE rogozhevaan;

ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/json-serde-1.3.8-jar-with-dependencies.jar;

DROP TABLE IF EXISTS kkt_table;

CREATE external TABLE kkt_table (
    id Struct < oid: String >,
    kktRegId String,
    subtype String,
    content Struct < userInn: String, totalSum: Bigint, dateTime: struct < date: Timestamp > >
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    'ignore.malformed.json' = 'true',
    'mapping.id' = '_id',
    'mapping.oid' = '$oid',
    'mapping.date' = '$date'
)
STORED AS TEXTFILE
LOCATION '/data/hive/fns2';

SELECT * FROM kkt_table LIMIT 50;
